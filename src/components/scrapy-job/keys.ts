// Copyright IBM Corp. 2020. All Rights Reserved.
// Node module: @loopback/cron
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT
import {BindingKey} from '@loopback/core';
// import {Builder} from 'selenium-webdriver';
import {ScrapyJobComponent} from './scrapy-job.component';

/**
 * Binding keys used by this component.
 */
export namespace ScrapyJobBindings {
  /**
   * Binding key for `CronComponent`
   */
  export const COMPONENT = BindingKey.create<ScrapyJobComponent>(
    'components.CronJobComponent',
  );

  /**
   * Namespace for cron jobs
   */
  export const SCRAPPERS = 'scrappers';
}

export namespace ScrapyJobTags {
  /**
   * Tag for repository bindings
   */
  export const SCRAPPER = 'scrapper';
}
