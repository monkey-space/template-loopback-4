import {
  ArtifactOptions,
  BaseArtifactBooter,
  BootBindings,
  booter,
} from '@loopback/boot';
import {
  config,
  CoreBindings,
  createBindingFromClass,
  inject,
} from '@loopback/core';
import {RestApplication} from '@loopback/rest';
import {ScrapyJobBindings, ScrapyJobTags} from '../keys';

@booter('scrappers')
export class ScrapyJobScrapperBooter extends BaseArtifactBooter {
  constructor(
    @inject(CoreBindings.APPLICATION_INSTANCE) public app: RestApplication,
    @inject(BootBindings.PROJECT_ROOT) projectRoot: string,
    @config() public scrapyJobScrapperConfig: ArtifactOptions = {},
  ) {
    super(
      projectRoot,
      // Set Controller Booter Options if passed in via bootConfig
      Object.assign({}, ScrapyJobScrapperDefaults, scrapyJobScrapperConfig),
    );
  }

  /**
   * Uses super method to get a list of Artifact classes. Boot each class by
   * binding it to the application using `app.controller(controller);`.
   */
  async load() {
    await super.load();
    this.classes.forEach(cls => {
      const scrapper = createBindingFromClass(cls, {
        // FIXME: Añadir binding para claves de scrapper
        namespace: ScrapyJobBindings.SCRAPPERS,
        type: ScrapyJobTags.SCRAPPER, // RepositoryTags.REPOSITORY
      }).tag(ScrapyJobTags.SCRAPPER);
      this.app.add(scrapper);
    });
  }
}

/**
 * Default ArtifactOptions for CronJobScrapperBooter.
 */
export const ScrapyJobScrapperDefaults: ArtifactOptions = {
  dirs: ['scrappers'],
  extensions: ['.scrapper.js'],
  nested: true,
};
