import {
  Component,
  config,
  CoreBindings,
  createBindingFromClass,
  inject,
} from '@loopback/core';
import {RestApplication} from '@loopback/rest';
import {ScrapyJobScrapperBooter} from './booters/scrapy-job.booter';

export class ScrapyJobComponent implements Component {
  bindings = [createBindingFromClass(ScrapyJobScrapperBooter)];

  constructor(
    @config({
      fromBinding: CoreBindings.APPLICATION_INSTANCE,
      propertyPath: 'rest.host',
    })
    public host: string,
    @config({
      fromBinding: CoreBindings.APPLICATION_INSTANCE,
      propertyPath: 'rest.port',
    })
    public port: string,
    @inject(CoreBindings.APPLICATION_INSTANCE)
    app: RestApplication,
  ) {
    console.log([host, port]);
    // const driver = new Builder().forBrowser('chrome');

    // app.bind(ScrapyJobBindings.DRIVER).toDynamicValue(() => driver);
  }
}
