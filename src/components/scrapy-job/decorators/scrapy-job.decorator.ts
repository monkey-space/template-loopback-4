import {Constructor, inject} from '@loopback/context';

export function scrapper(scrapperName: Constructor<unknown>) {
  return inject(`scrappers.${scrapperName.name}`);
}
