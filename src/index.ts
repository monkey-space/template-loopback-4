import {ApplicationConfig, MonkeyProjectApplication} from './application';

export * from './application';

export async function main(options: ApplicationConfig = {}) {
  const app = new MonkeyProjectApplication(options);
  await app.boot();
  await app.start();

  const url = app.httpServer.url;
  console.log(`Server is running at ${url}`);
  console.log(`Try ${url}/ping`);

  return app;
}

if (require.main === module) {
  const port = process.env.PORT ?? 3000;
  const host = process.env.HOST ?? 'localhost';
  // Run the application
  const config: ApplicationConfig = {
    rest: {
      port,
      host,
      gracePeriodForClose: 5000, // 5 seconds
      openApiSpec: {
        // useful when used with OpenAPI-to-GraphQL to locate your application
        setServersFromRequest: true,
      },
      listenOnStart: false,
    },
    websocket: {
      port,
    },
  };
  main(config).catch(err => {
    console.error('Cannot start the application.', err);
    process.exit(1);
  });
}
