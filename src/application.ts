import {BootMixin} from '@loopback/boot';
import {ApplicationConfig} from '@loopback/core';
import {RepositoryMixin} from '@loopback/repository';
import {RestExplorerComponent} from '@loopback/rest-explorer';
import {ServiceMixin} from '@loopback/service-proxy';
// import debugFactory from 'debug';
// import {WebsocketApplication, WebsocketControllerBooter} from 'monkey-socket.io';
import path from 'path';
import {WebsocketApplication} from './components/websockets/websocket.application';
import {WebsocketControllerBooter} from './components/websockets/websocket.booter';
import {MySequence} from './sequence';

export {ApplicationConfig};

// const debug = debugFactory('loopback:socketio:demo');

export class MonkeyProjectApplication extends BootMixin(
  ServiceMixin(RepositoryMixin(WebsocketApplication)),
) {
  constructor(options: ApplicationConfig = {}) {
    super(options);
    // Set up the custom sequence
    this.sequence(MySequence);

    // Set up default home page
    this.static('/', path.join(__dirname, '../public'));

    // Customize @loopback/rest-explorer configuration here
    // this.configure(RestExplorerBindings.COMPONENT).to({
    //   path: '/explorer',
    // });
    this.component(RestExplorerComponent);
    // this.component(ScrapyJobComponent);
    this.booters(WebsocketControllerBooter);

    // this.component(MonkeySocketIoComponent);

    this.projectRoot = __dirname;
    // Customize @loopback/boot Booter Conventions here
    this.bootOptions = {
      controllers: {
        // Customize ControllerBooter Conventions here
        dirs: ['controllers'],
        extensions: ['.controller.js'],
        nested: true,
      },
    };
  }
}
