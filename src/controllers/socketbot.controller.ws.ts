/* eslint-disable @typescript-eslint/no-explicit-any */
// import {Socket} from 'socket.io';
// import {ws} from '../websockets/decorators/websocket.decorator';
import {Socket} from 'socket.io';
import {ws} from '../components/websockets/decorators/websocket.decorator';

/**
 * A demo controller for websocket
 */
// @ws({name: 'socketBotNsp', namespace: /^\/scrapy\/\d+$/}
@ws({name: 'scrapperNsp', namespace: 'scrapy'})
export class SocketBotControllerWs {
  constructor(
    @ws.socket() // Equivalent to `@inject('ws.socket')`
    private socket: Socket,
  ) {}

  /**
   * The method is invoked when a client connects to the server
   * @param socket
   */
  @ws.connect()
  connect(socket: Socket) {
    console.log('Client connected: %s', this.socket.id);

    // rooms
    socket.join('events');

    socket.join('signin');
  }

  @ws.subscribe('test scrapy')
  test(data: any) {
    this.socket.nsp.emit('test scrapy', data);
  }

  /**
   * Register a handler for 'chat message' events
   * @param msg
   */
  @ws.subscribe('on_scrap_articles')
  // @ws.emit('namespace' | 'requestor' | 'broadcast')
  async scrapArticles(data: any) {
    this.socket.nsp.emit('on_scrap_articles', data);
  }

  @ws.subscribe('update_articles_list')
  updateArticlesList(data: any) {
    this.socket.nsp.emit('updateArticlesList', data);
    this.socket.nsp.emit('updateAuctionsList');
  }

  @ws.subscribe('updateAuction')
  update(data: any) {
    this.socket.nsp.emit('updateAuction', data);
  }

  @ws.subscribe('on_delete_auction')
  deleteAuction(data: any) {
    this.socket.nsp.emit('on_delete_auction', data);
  }

  @ws.subscribe('on_pause_auction')
  pauseAuction(data: any) {
    this.socket.nsp.emit('on_pause_auction', data);
  }

  @ws.subscribe('on_restart_auction')
  restartAuction(data: any) {
    this.socket.nsp.emit('on_restart_auction', data);
  }

  /**
   * Register a handler for all events
   * @param msg
   */
  @ws.subscribe(/.+/)
  logMessage(...args: unknown[]) {
    console.log('Message: %s', args.toString());
  }

  /**
   * The method is invoked when a client disconnects from the server
   * @param socket
   */
  @ws.disconnect()
  disconnect() {
    console.log('Client disconnected: %s', this.socket.id);
  }
}
