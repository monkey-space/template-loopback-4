/* eslint-disable @typescript-eslint/no-explicit-any */
import {Socket} from 'socket.io';
import {ws} from '../components/websockets/decorators/websocket.decorator';

@ws('/')
export class ServerControllerWs {
  constructor(
    @ws.socket() // Equivalent to `@inject('ws.socket')`
    private socket: Socket,
  ) {}

  /**
   * The method is invoked when a client connects to the server
   * @param socket
   */
  @ws.connect()
  connect(socket: Socket) {
    console.log('Client connected to Server: %s', this.socket.id);

    // rooms connection
    socket.join('events');
  }

  @ws.subscribe('action event')
  restartAuction(data: any) {
    this.socket.nsp.emit('action event', data);
  }

  /**
   * Register a handler for all events
   * @param msg
   */
  @ws.subscribe(/.+/)
  logMessage(...args: unknown[]) {
    console.log('Message: %s', args.toString());
  }

  /**
   * The method is invoked when a client disconnects from the server
   * @param socket
   */
  @ws.disconnect()
  disconnect() {
    console.log('Client disconnected: %s', this.socket.id);
  }
}
